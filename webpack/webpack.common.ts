import * as path from 'path';

const commonOptions = {
    outputPath: path.resolve(__dirname, '..', 'dist'),
    srcPath: path.resolve(__dirname, '..', 'src'),
    htmlIndexPath: path.resolve(__dirname, '..', 'index.html'),
    entry: {
        'client': path.resolve(__dirname, '..', 'src/index.tsx'),
    }
}

export default commonOptions;
