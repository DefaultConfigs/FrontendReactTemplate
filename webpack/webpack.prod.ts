import {DefaultProdOptions, WebpackConfigFactory} from 'frontend-webpack-config-creator';
import CommonOptions from './webpack.common';

const config = WebpackConfigFactory(Object.assign(DefaultProdOptions, CommonOptions))

export default config;
