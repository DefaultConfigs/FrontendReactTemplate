import {DefaultDevOptions, WebpackConfigFactory} from 'frontend-webpack-config-creator';
import CommonOptions from './webpack.common';

const config = WebpackConfigFactory(Object.assign(DefaultDevOptions, CommonOptions))

export default config;
